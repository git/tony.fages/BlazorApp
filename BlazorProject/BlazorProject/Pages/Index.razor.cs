﻿using System;
using BlazorProject.Components;
using BlazorProject.Models;
using BlazorProject.Services;
using Microsoft.AspNetCore.Components;

namespace BlazorProject.Pages
{
    public partial class Index
    {
        
        private Cake CakeItem = new Cake
        {
            Id = 1,
            Name = "Black Forest",
            Cost = 50
        };

        public List<Cake> Cakes { get; set; }

        [Inject]
        public IDataService DataService { get; set; }

        public List<Item> Items { get; set; } = new List<Item>();

        private List<CraftingRecipe> Recipes { get; set; } = new List<CraftingRecipe>();

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            base.OnAfterRenderAsync(firstRender);

            if (!firstRender)
            {
                return;
            }

            Items = await DataService.List(0, await DataService.Count());
            Recipes = await DataService.GetRecipes();

            StateHasChanged();
        }

        public void LoadCakes()
        {
            Cakes = new List<Cake>
        {
            // items hidden for display purpose
            new Cake
            {
                Id = 1,
                Name = "Red Velvet",
                Cost = 60
            },
        };
        }

    }
}

